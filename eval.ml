open Syntax
;;

let ext env v =
  Array.append [|v|] env
;;

let lookup x env =
  let index = int_of_string x in
    env.(index)
;;

let rec eval exp env =
  let apply_op op_type p q=
    match op_type, p, q with
      | Add, Const_Val(n1), Const_Val(n2) -> Const_Val(n1+n2)
      | Sub, Const_Val(n1), Const_Val(n2) -> Const_Val(n1-n2)
      | Mul, Const_Val(n1), Const_Val(n2) -> Const_Val(n1*n2)
      | Div, Const_Val(n1), Const_Val(n2) -> Const_Val(n1/n2)
      |   _, _, _ -> raise (Eval_Error "Binary operation error")
  in
    match exp with
      | Var(x)       -> 
          lookup x env

      | Bin_Op(op_type, t, u) -> 
          let q = eval u env in
          let p = eval t env in
            apply_op op_type p q

      | Const(n)     -> Const_Val(n)

      | If_Zero(t, u, v) ->
          begin
            match (eval t env) with
              | Const_Val(0) -> eval u env
              | Const_Val(n) -> eval v env
              | _ -> raise (Eval_Error "Ifz argument should be Const")
          end

      | Fix_Fun(f, x, t) -> Rec_Clos(f, x, t, env)

      | Func(x, t) -> Rec_Clos("$", x, t, env)

      | Let_In(x, t, u) ->
          let w  = eval t env in
          let e1 = ext env w in
            eval u e1

      | App(t,u)   -> 
          let funcval = eval t env in
            match funcval with
              | Rec_Clos(f, x, t1, e1) -> 
                  let w  = eval u env in
                  let e2 = ext e1 funcval in
                  let e3 = ext e2 w in
                    eval t1 e3
              | _ -> raise (Eval_Error "Application error")
;;
