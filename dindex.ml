open Syntax
;;

let diconv exp env =  
  let di_ext env x = x :: env in
  let di_lookup x env =
    let rec iter x env count =
      match env with
        | [] -> raise (Eval_Error ("Unbounded variable: " ^ x))
        | y::tl -> 
            if x = y then count 
            else iter x tl (count + 1)
    in
      iter x env 0
  in
  let rec subst exp env count=  
    match exp with
      | Var(x) ->
          let v = di_lookup x env in
            Var(string_of_int(v))
      | Bin_Op(op, t, u) ->
          Bin_Op(op, (subst t env count), (subst u env count))
      | Const(t) ->
          Const(t)
      | If_Zero(t, u, v) ->
          If_Zero((subst t env count), (subst u env count), (subst v env count))
      | Fix_Fun(f, v, t) ->
          let env1 = di_ext env  f in
          let env2 = di_ext env1 v in
            Fix_Fun ( string_of_int(count),
                      string_of_int(count+1), 
                     (subst t env2 (count+2)))
      | Func(v, t) ->
          let env1 = di_ext env v in
            Func( string_of_int(count), 
                 (subst t env1 (count+1)))
      | Let_In(t, u, v) ->
          let env1 = di_ext env t in
            Let_In( string_of_int(count), 
                    (subst u env count), 
                    (subst v env1 (count+1)))
      | App(t, u) ->
          App((subst t env count), (subst u env count))
  in
    subst exp env 0
;;

