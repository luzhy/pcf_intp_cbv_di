open Syntax;;

let rec pprint_term tm counter=
  match tm with
    | Var(x)       -> 
        begin 
          print_string "x";
          print_int (counter - (int_of_string x));
          print_string " "
        end

    | Bin_Op(op_type, t, u) -> 
        let print_op op =
          match op with
            | Add -> print_string "+ "
            | Sub -> print_string "- "
            | Mul -> print_string "* "
            | Div -> print_string "/ "
        in
          begin
            print_string "( ";
            pprint_term t counter;
            print_op op_type;
            pprint_term u counter;
            print_string ") "
          end

    | Const(n)     -> 
        begin
          print_int n;
          print_string " "
        end

    | If_Zero(t, u, v) ->
        begin
          print_string "ifz ";
          pprint_term t counter;
          print_string "then ";
          pprint_term u counter;
          print_string "else ";
          pprint_term v counter
        end

    | Fix_Fun(f, x, t) ->
        begin
          print_string "fixfun ";
          print_string "x";
          print_string f;
          print_string " ";
          print_string "x";
          print_string x;
          print_string " ";
          pprint_term t (int_of_string(x))
        end

    | Func(x, t) -> 
        begin
          print_string "fun ";
          print_string "x";
          print_string x;
          print_string " -> ";
          pprint_term t (int_of_string(x))
        end

    | Let_In(x, t, u) ->
        begin
          print_string "let ";
          print_string "x";
          print_string x;
          print_string " = ";
          pprint_term t counter;
          print_string "in ";
          pprint_term u (int_of_string(x)) 
        end

    | App(t,u)   -> 
        begin
          print_string "( ";
          pprint_term t counter;
          pprint_term u counter;
          print_string ") ";
        end
;;

let pprint_value v =
  match v with
    | Const_Val(n)  ->
        begin
          print_string "<Const_Val> = ";
          print_string (string_of_int(n));
          print_endline ""
        end

    | Rec_Clos(f, x, t, env) ->
        begin
          print_string "<Rec_Clos>   = ";
          print_string "fixfun ";
          print_string "x";
          print_string f;
          print_string " ";
          print_string "x";
          print_string x;
          print_string " -> ";
          pprint_term t (int_of_string(x));
          print_endline ""
        end
;;
