(* main.ml *)

open Syntax ;;
open Eval ;;
open Dindex ;;

let debug str =
  let decl = Iparse.main Lex.token (Lexing.from_string str) in
  let di_decl = diconv decl [] in
    eval di_decl [||]
;;

let rec repl env chn = 
  print_string "> ";
  flush stdout;
  try
  let decl = Iparse.main Lex.token (Lexing.from_channel chn) in
  let di_decl = diconv decl [] in
  let res  = eval di_decl env in
    Printf.printf "- : val = ";
    Util.pprint_value res;
    print_newline();
    repl env chn
  with 
    | Lex.Lexer_Error s    -> 
        begin
          print_endline ("Error: " ^ s);
          repl env stdin
        end
    | Syntax.Eval_Error s    -> 
        begin
          print_endline ("Error: " ^ s);
          repl env stdin
        end
    | Syntax.Parser_Error s -> 
        begin
          print_endline ("Error: " ^ s);
          repl env stdin
        end
    | Syntax.Parser_EOF -> print_endline "Exit"
;;

let intp () = 
  begin
    print_endline "PCF Interpreter (Call by Value; Recursive Closure; de Brujin Index)";
    repl [||] stdin
  end
;;

intp ()
